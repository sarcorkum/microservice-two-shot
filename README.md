# Wardrobify

Team:


* James- Shoes
* Sarah - Hats


## Design
Open and run this in your terminal.
1) docker volume create two-shot-pgdata
2) docker-compose build
3) docker-compose up!
![Alt text](image-1.png)
4) Go to http://localhost:3000/ in order to see app in browser
## Shoes microservice
1) This Shoes microservice, is utilized to organize the shoes.
In my Shoes microservice. I have created the BinVO model that has the properties. which is kind of like a representation of the Bin Model in the wardrobe.
-import_href.
-closet_name
-bin_number
-bin size.

I've also created the Shoe model that has the properties of
-manufacturer- manufacturer of the shoes.
-model name- name of the shoe
-color- color of the shoe
-picture url- url to a pic of the shoe
-bin, which is the location. -storage of the bin where the shoe is located in the wardrobe.

In order for the app to work, 'shoes_rest.apps.ShoesApiConfig' must be added to the shoes_projects in the installed apps section.

Poller gets data from the bin model in the wardrobe through my binvo model.

endpoint lists: HTTP Responses
create shoes:  http://localhost:8080/api/shoes/   POST
list shoes:      http://localhost:8080/api/shoes/   GET
detail_shoes:  http://localhost:8080/api/shoes/<int>:pk GET
delete shoes: http://localhost:8080/api/shoes/

create bin GET	http://localhost:8100/api/bins/	 create bin
delte bin DELETE	http://localhost:8100/api/bins/<int:pk>/	Deletes a single bin

## Hats microservice

The hats microservice has a LocationVO model and a Hat model. The LocationVO model is a value object based on the wardrobe's Location model. The Location model and LocationVO model are in two different bounded contexts. The poller is pulling the location data from the wardrobe API and giving it to the LocationVO model. The Hat model in the hats microservice has a property named location. This location data comes from the LocationVO model in the hats microservice.

Endpoints:
- List hats: http://localhost:8090/api/hats/
- Create hat: http://localhost:8090/api/hats/
- Delete hat: http://localhost:8090/api/hats/<int:pk>/
- Show hat details: http://localhost:8090/api/hats/<int:pk>/
- Create location: http://localhost:8100/api/locations/
- List locations: http://localhost:8100/api/locations/
