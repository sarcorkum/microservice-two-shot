from django.db import models
from django.urls import reverse
# Create your models here.


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=200)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)
    import_href = models.CharField(max_length=200, unique=True) #allows us to reference back to original object

    def get_api_url(self):
        return reverse("api_locations")


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(max_length=200)
    location = models.ForeignKey(
        LocationVO,
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return f"{self.style_name}"

    def get_api_url(self):
        return reverse("api_show_hats", kwargs={"pk": self.pk})
