from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse

from common.json import ModelEncoder
from .models import BinVO, Shoe

# Create your views here.
class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href",
                  "closet_name",
                  "bin_number",
                  "bin_size",
                ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_URL",
        "bin",
        ]

    encoders ={"bin":BinVODetailEncoder()
               }

    def get_extra_data(self, o):
        return {"import_href": o.bin.import_href} # looks at binvodetailencoder and got confused since no shoe property.

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_URL",
        "bin",
    ]
    encoders ={"bin":BinVODetailEncoder()
               }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id = None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content=json.loads(request.body)
        try:
            bin_href = content["bin"]       # Getting the href for the bin. Sarah is getting the id.
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_show_shoes(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
