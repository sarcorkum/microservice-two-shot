import React, { useState, useEffect } from 'react';

function ShoeForm() {

    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModelName] = useState('');
    const [color, setColor]  = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);


    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setBins(data.bins);
        }
    }

    useEffect(() => {
    fetchData();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.color = color;
        data.picture_URL = picture_url;
        data.bin = bin;
        console.log(data);



    const ShoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    };


    const response = await fetch(ShoeUrl, fetchConfig);
    if (response.ok) {
        const newShoe= await response.json();
        console.log(newShoe);

        setManufacturer('');
        setModelName('');
        setColor('');
        setPictureUrl('');
        setBin('');
        }
    }

    const handleManufacturerChange= async (event) => {
        const { value } = event.target;
        setManufacturer(value);
    }

    const handleModelNameChange= async (event) => {
        const { value } = event.target;
        setModelName(value);
    }
    const handleColorChange= async (event) => {
        const { value } = event.target;
        setColor(value);
    }

    const handlePictureUrlChange= async (event) => {
        const { value } = event.target;
        setPictureUrl(value);
    }

    const handleBinChange= async (event) => {
        const { value } = event.target;
        setBin(value);
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create Shoes</h1>
                    <form onSubmit={handleSubmit} id="create-shoes-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleModelNameChange} value={model_name} placeholder="Model name" required type="text" name="model_name" id="model_name" className="form-control" />
                            <label htmlFor="model_name">Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} value={picture_url} placeholder="PictureUrl" required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture Url</label>
                            <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                                <option value="">Choose a bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.closet_name} value={bin.href}>{bin.closet_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ShoeForm
