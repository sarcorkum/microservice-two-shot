import React, {useEffect, useState} from 'react';

function HatForm(props) {

  const [styleName, setStyleName] = useState('');
  const handleStyleNameChange = (event) => {
      const value = event.target.value;
      setStyleName(value);
  }

  const [fabric, setFabric] = useState('');
  const handleFabric = (event) => {
      const value = event.target.value;
      setFabric(value);
  }

  const [color, setColor] = useState('');
  const handleColor = (event) => {
      const value = event.target.value;
      setColor(value);
  }

  const [pictureUrl, setPictureUrl] = useState('');
  const handlePictureUrl = (event) => {
      const value = event.target.value;
      setPictureUrl(value);
  }

  const [location, setLocation] = useState('');
  const handleLocationChange = (event) => {
      const value = event.target.value;
      setLocation(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.style_name = styleName;
    data.color = color;
    data.fabric = fabric;
    data.picture_url = pictureUrl;
    data.location = location;
    console.log(data);

    const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
          const newHat = await response.json();
          console.log(newHat);

            setStyleName('');
            setFabric('');
            setColor('');
            setPictureUrl('');
            setLocation('');
            window.location.reload()
        }
      }

    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setLocations(data.locations);
        }
        }

    useEffect(() => {
      fetchData();
    }, []);


  return(
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleStyleNameChange} placeholder="Style name" value={styleName} required type="text" name="style_name" id="style_name" className="form-control"/>
              <label htmlFor="style_name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFabric} placeholder="Fabric" value={fabric} required type="text" name="fabric" id="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColor} placeholder="Color" value={color} required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureUrl} placeholder="Picture URL" value={pictureUrl} required type="text" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required id="location" value={location} name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map((location, index) => {
                    return (
                        <option key={location.id + index} value={location.id}>
                        {location.id}
                        </option>
                    )
                  })}
                </select>
              </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default HatForm;
