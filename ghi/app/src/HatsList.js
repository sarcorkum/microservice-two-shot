
function HatsList(props) {

  const deleteHat = async(id) => {
    const hatUrl = `http://localhost:8090/api/hats/${id}`
    const fetchConfig = {
      method: "delete",
      headers: {
          'Content-Type': 'application/json'
      }
    }
    const response = await fetch(hatUrl, fetchConfig)
    if (response.ok) {
      window.location.reload()
    }
  }
    return (
      <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Style Name</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Picture URL</th>
            <th>Location</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map((hat, index) => {
            return (
              <tr key={hat.href + index}>
                <td>{ hat.style_name }</td>
                <td>{ hat.fabric }</td>
                <td>{ hat.color }</td>
                <td>{ hat.picture_url }</td>
                <td>{ hat.location.closet_name }</td>
                <td>
                  <button onClick={(e) => deleteHat(hat.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
  }
  export default HatsList;
