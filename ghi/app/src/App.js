import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatForm from './HatForm';

function App(props) {
  const [ shoes, setShoes ] = useState([]);
  const [ bins, setBins ] = useState([]);
  const [ hats, setHats ] = useState([]);
  const [ locations, setLocations ] = useState([]);

//   async function getShoes() {
//     const response = await fetch('http://localhost:8080/api/shoes/');
//     if (response.ok) {
//       const { shoes } = await response.json();
//       setShoes(shoes);
//     } else {
//       console.error('An error occurred fetching the data')
//     }
//   }
// }

//   async function getBins() {
//     const url = 'http://localhost:8100/api/bins/';
//     const response = await fetch(url);
//     if (response.ok) {
//       const data = await response.json();
//       setBins(data.bins);
//   const [ hats, setHats ] = useState([]);
//     }

  async function getHats() {
    const response = await fetch('http://localhost:8090/api/hats/');
    if (response.ok) {
      const { hats } = await response.json();
      setHats(hats);
      console.log(hats)
    } else {
      console.error("An error occurred fetching the data")
    }
  }
  async function getLocations() {
    const url = "http://localhost:8100/api/locations/";
    const response = await fetch(url);
    if (response.ok) {
      const {locations} = await response.json ();
      setLocations(locations);
      console.log("locations:", locations)
    }
  }

  // useEffect(() => {
  //   getShoes();
  //   getBins();
  //   getHats();
  //   getLocations();
  // }, [])




// function App(props) {
  // const [ shoes, setShoes ] = useState([]);
  // const [ bins, setBins ] = useState([]);
// get shoes is pulling data from line 16.
  async function getShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  async function getBins() {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const { bins } = await response.json();
      setBins(bins);
      console.log(bins)
    }}

  // async function getHats() {
  //   const response = await fetch('http://localhost:8090/api/hats/');
  //   if (response.ok) {
  //     const { hats } = await response.json();
  //     setHats(hats);
  //   } else {
  //     console.error("An error occurred fetching the data")
  //   }
  // }

  useEffect(() => {
    getShoes();
    getBins();
    getHats();
    getLocations();
  }, [])

  if (shoes === undefined) {
    return null;
  }
  if (hats === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="" element={<HatsList hats={hats}/>} />
            <Route path="new" element={<HatForm/>} />
          </Route>
          <Route path="shoes">
          <Route path="" element={<ShoesList shoes={shoes}/>} />
          {/* On line 60 pulling data into function. to be able use the props from the shoes list file. because it child. this is what's mapped over to shoesList. that's why props.shoes.map insetead of props.shoe.map Shoes is a child and has access to props */}
          <Route path="new" element={<ShoeForm/>} />
        </Route>
        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;
