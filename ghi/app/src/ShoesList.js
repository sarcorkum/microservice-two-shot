function ShoesList(props) {
  const deleteShoe = async (id) => {
    const ShoeUrl = `http://localhost:8080/api/shoes/${id}`
    const fetchConfig = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const response = await fetch(ShoeUrl, fetchConfig)
    if (response.ok) {
      window.location.reload()
    }
  }

    return (

      <table className="table table-striped">

        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin Location</th>
            <th>Delete</th>
          </tr>
        </thead>

        <tbody>
          {props.shoes.map((shoe, index) => {
            return (
              <tr key={shoe.id + index}>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.picture_URL }</td>
                <td>{ shoe.bin.closet_name}</td>
                <td> <button onClick={(e) => deleteShoe(shoe.id)} >Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoesList;
